﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
namespace Continuity2Assessment.Pages;

public abstract class BasePage
{
    protected IWebDriver _driver;
    protected WebDriverWait _driverWait;

    public BasePage(IWebDriver driver)
    {
        _driver = driver;
        _driverWait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10))
        {
            PollingInterval = TimeSpan.FromSeconds(1)
        };
    }

    protected void ClickButtonWithJS(IWebElement button)
    {
        IJavaScriptExecutor jsExecutor = (IJavaScriptExecutor)_driver;
        jsExecutor.ExecuteScript("arguments[0].click()", button);
    }
}