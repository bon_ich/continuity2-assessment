﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
namespace Continuity2Assessment.Pages;

public class HomePage : BasePage
{
    private readonly string _url = "https://www.qatarairways.com/en-ua/homepage.html";

    // selectors for elements in shadowed DOM
    private readonly string _fromFieldSelector = "#mat-input-0";
    private readonly string _toFieldSelector = "#mat-input-1";
    private readonly string _searchButtonSelector = ".flightsearh-button";
    private readonly string _departureDateFieldSelector = "#dpFromDate";
    private readonly string _returnDateFieldSelector = "#dpToDate";
    private readonly string _passengersDropdownSelector = "#passenger-content";
    private readonly string _adultCounterIncreaseButtonSelector = "button[aria-label='Increase number of Adult passengers']";
    private readonly string _passengersDropdownConfirmButtonSelector = ".col-xs-12.col-sm-12.paxconfirm";

    private By _acceptAllCookiesButtonBy => By.Id("cookie-accept-all");
    private By _airportNameBy = By.CssSelector(".iatacode");
    private IWebElement _fromFieldAirportsResults => _driver.FindElement(By.CssSelector("#mat-tab-content-0-0"));
    private IWebElement _toFieldAirportsResults => _driver.FindElement(By.CssSelector("#mat-tab-content-1-0"));

    public HomePage(IWebDriver driver) : base(driver)
    {
    }

    public HomePage Open()
    {
        _driver.Navigate().GoToUrl(_url);
        return this;
    }

    protected IWebElement FindElementInShadowDOM(string cssSelector)
    {
        ISearchContext shadow = _driver.FindElement(By.XPath("//*[@ng-version='14.2.12']")).GetShadowRoot();
        return shadow.FindElement(By.CssSelector(cssSelector));
    }

    public HomePage AcceptAllCookies()
    {
        _driverWait.Until(ExpectedConditions.ElementToBeClickable(_acceptAllCookiesButtonBy)).Click();
        _driverWait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("cookie-close-accept-all")));
        return this;
    }

    public HomePage SelectFromPlace(string city, out string airport)
    {
        var fromField = FindElementInShadowDOM(_fromFieldSelector);
        _driverWait.Until(ExpectedConditions.ElementToBeClickable(fromField));
        fromField.SendKeys(city);
        Thread.Sleep(TimeSpan.FromSeconds(0.5f));
        IWebElement firstAirport = _fromFieldAirportsResults.FindElements(By.CssSelector("mat-option"))[0];
        airport = firstAirport.FindElement(_airportNameBy).Text;
        firstAirport.Click();
        return this;
    }

    public HomePage EnterToPlace(string city, out string airport)
    {
        var toField = FindElementInShadowDOM(_toFieldSelector);
        _driverWait.Until(ExpectedConditions.ElementToBeClickable(toField));
        toField.SendKeys(city);
        Thread.Sleep(TimeSpan.FromSeconds(0.5f));
        IWebElement firstAirport = _toFieldAirportsResults.FindElements(By.CssSelector("mat-option"))[0];
        airport = firstAirport.FindElement(_airportNameBy).Text;
        firstAirport.Click();
        return this;
    }

    public HomePage EnterDepartureDate(string date)
    {
        var departureDateField = FindElementInShadowDOM(_departureDateFieldSelector);
        departureDateField.Clear();
        departureDateField.SendKeys(date);
        return this;
    }

    public HomePage EnterReturnDate(string date)
    {
        var returnDateField = FindElementInShadowDOM(_returnDateFieldSelector);
        returnDateField.Clear();
        returnDateField.SendKeys(date);
        return this;
    }

    public HomePage SetAdultPassengersCount(int passengersCount)
    {
        FindElementInShadowDOM(_passengersDropdownSelector).Click();
        var adultsCountDropdown = FindElementInShadowDOM(_adultCounterIncreaseButtonSelector);
        for (int i = 0; i < passengersCount - 1; i++)
        {
            adultsCountDropdown.Click();
        }
        return this;
    }

    public HomePage ApplyPassengersConfiguration()
    {
        var confirmButton = FindElementInShadowDOM(_passengersDropdownConfirmButtonSelector);
        _driverWait.Until(ExpectedConditions.ElementToBeClickable(confirmButton));
        ClickButtonWithJS(confirmButton);
        return this;
    }

    public void ClickSearchButton()
    {
        IWebElement button = FindElementInShadowDOM(_searchButtonSelector);
        button.Click();
    }
}