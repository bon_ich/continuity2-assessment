﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
namespace Continuity2Assessment.Pages;

public class SearchResultsPage : BasePage
{
    private readonly string _economyConvenienceFareCardXpath = "//div[contains(@class,'at-ECONOMY_CONVENIENCE')]";
    private readonly string _businessComfortFareCardXpath = "//div[contains(@class,'at-BUSINESS_COMFORT')]";
    private readonly string _selectFareButtonXpath = "//button[@class='secondary at-select-fare-btn']";

    private readonly By _searchResultsContainerBy = By.XPath("//booking-flight-selection-page");
    private readonly By _farePriceBy = By.CssSelector("h3[class*='fare-card__price']");
    private readonly By _economyFlightButtonBy = By.CssSelector("a[aria-label^='Economy']");
    private readonly By _businessFlightButtonBy = By.CssSelector("a[aria-label^='Business']");
    private readonly By _economyConvenienceFareButtonBy, _businessComfortFareButtonBy;
    private readonly By _foundFlightsBy = By.XPath("//booking-flight-result-card");
    private readonly By _flightFromAirport = By.CssSelector(".at-flight-card-origin-code");
    private readonly By _flightToAirport = By.CssSelector(".at-flight-card-destination-code");

    public SearchResultsPage(IWebDriver driver) : base(driver)
    {
        _economyConvenienceFareButtonBy = By.XPath(_economyConvenienceFareCardXpath + _selectFareButtonXpath);
        _businessComfortFareButtonBy = By.XPath(_businessComfortFareCardXpath + _selectFareButtonXpath);
    }

    public bool IsResultsPageOpened()
    {
        IWebElement _resultsContainer = _driverWait.Until(ExpectedConditions.ElementIsVisible(_searchResultsContainerBy));
        return _resultsContainer.Displayed;
    }

    public SearchResultsPage SelectFlight(bool isBusinessFlight, FlightType flightType, out int selectedFlightPrice, bool isLastFlightSelected = false)
    {
        IWebElement flightTypeButton = _driverWait.Until(ExpectedConditions.ElementIsVisible(
            isBusinessFlight
                ? _businessFlightButtonBy
                : _economyFlightButtonBy));
        _driverWait.Until(ExpectedConditions.TextToBePresentInElement(
            flightTypeButton,
            isBusinessFlight
                ? "Business"
                : "Economy"));
        flightTypeButton.Click();
        SelectFlightType(flightType, out selectedFlightPrice);

        if (!isLastFlightSelected)
            _driverWait.Until(ExpectedConditions.StalenessOf(flightTypeButton));
        return this;
    }

    private void SelectFlightType(FlightType flightType, out int selectedFlightPrice)
    {
        IWebElement? selectFareButton = null;
        selectedFlightPrice = 0;

        IWebElement fareCard;
        switch (flightType)
        {
            case FlightType.EconomyConvenience:
                fareCard = _driverWait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_economyConvenienceFareCardXpath)));
                selectFareButton = _driver.FindElement(_economyConvenienceFareButtonBy);
                _driverWait.Until(ExpectedConditions.ElementToBeClickable(selectFareButton));
                selectedFlightPrice = GetFlightPriceFromFareCard(fareCard);
                break;
            case FlightType.BusinessComfort:
                fareCard = _driverWait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_businessComfortFareCardXpath)));
                selectFareButton = _driver.FindElement(_businessComfortFareButtonBy);
                _driverWait.Until(ExpectedConditions.ElementToBeClickable(selectFareButton));
                selectedFlightPrice = GetFlightPriceFromFareCard(fareCard);
                break;
        }
        if (selectFareButton != null)
            ClickButtonWithJS(selectFareButton);
    }

    public void ConfirmTrip()
    {
        IWebElement confirmButton = _driverWait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".toast__cta > button")));
        confirmButton.Click();
    }

    private int GetFlightPriceFromFareCard(IWebElement fareCard)
    {
        string priceText = fareCard.FindElement(_farePriceBy).Text;
        return Int32.Parse(priceText.Substring(1).Replace(",", ""));
    }
    public void VerifyFlightsAirports(string fromAirport, string toAirport)
    {
        ReadOnlyCollection<IWebElement> flights = null;
        _driverWait.Until(d =>
        {
            flights = _driver.FindElements(_foundFlightsBy);

            if (flights.Count != 0)
                return true;
            return false;
        });
        foreach (var f in flights!)
        {
            string from = f.FindElement(_flightFromAirport).Text;
            string to = f.FindElement(_flightToAirport).Text;
            Assert.That(from, Is.EqualTo(fromAirport));
            Assert.That(to, Is.EqualTo(toAirport));
        }
    }
}
public enum FlightType
{
    EconomyConvenience,
    BusinessComfort
}