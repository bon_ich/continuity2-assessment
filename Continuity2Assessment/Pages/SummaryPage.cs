﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
namespace Continuity2Assessment.Pages;

public class SummaryPage : BasePage
{
    private By _totalPriceSelector = By.CssSelector(".tripSummary__pax-total__price");
    private By _selectedFlights = By.CssSelector(".tripSummary__itinery > li");
    private By _passengersTotal = By.CssSelector(".tripSummary__pax-total__label");
    private By _datesSummary = By.CssSelector(".tripSummary__dates");
    private IWebElement _totalPrice => _driver.FindElement(_totalPriceSelector);
    public SummaryPage(IWebDriver driver) : base(driver)
    {
    }

    public int GetTotalPrice()
    {
        _driverWait.Until(ExpectedConditions.ElementIsVisible(_totalPriceSelector));
        return Int32.Parse(_totalPrice.Text.Substring(1).Replace(",", ""));
    }

    public void VerifyFlightsList(int expectedCount, string[] expectedFlightsDescriptions)
    {
        List<IWebElement> flights = _driver.FindElements(_selectedFlights).ToList();
        Assert.That(expectedCount, Is.EqualTo(flights.Count));
        for (int i = 0; i < flights.Count; i++)
        {
            Assert.That(expectedFlightsDescriptions[i], Is.EqualTo(flights[i].Text));
        }
    }

    public void VerifyFlightDates(string fromDate, string toDate)
    {
        var dates = _driver.FindElement(_datesSummary).Text.Split(" - ");
        Assert.That(dates[0].Contains(fromDate), Is.EqualTo(true));
        Assert.That(dates[1].Contains(toDate), Is.EqualTo(true));
    }

    public string GetPassengersTotal()
    {
        return _driver.FindElement(_passengersTotal).Text;
    }

    public int GetPassengersCount()
    {
        return _driver.FindElements(By.XPath("//booking-passenger-list//li")).Count;
    }
}