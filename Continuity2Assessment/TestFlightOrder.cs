using Continuity2Assessment.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
namespace Continuity2Assessment;

public class Tests
{
    private IWebDriver _driver;

    [SetUp]
    public void Setup()
    {
        ChromeOptions options = new ChromeOptions();
        options.AddArgument("--disable-blink-features=AutomationControlled");
        _driver = new ChromeDriver(options: options);
        _driver.Manage().Window.Maximize();
    }

    [Test]
    public void TestSuccessfulReturnOrder()
    {
        string fromCity = "London";
        string toCity = "Tokyo";
        string departureDate = "18 Nov";
        string returnDate = "26 Nov";
        int passengersCount = 2;

        HomePage homePage = new HomePage(_driver);
        homePage.Open()
            .AcceptAllCookies()
            .SelectFromPlace(fromCity, out string fromAirport)
            .EnterToPlace(toCity, out string toAirport)
            .EnterDepartureDate(departureDate + " " + DateTime.Now.Year)
            .EnterReturnDate(returnDate + " " + DateTime.Now.Year)
            .SetAdultPassengersCount(passengersCount)
            .ApplyPassengersConfiguration()
            .ClickSearchButton();

        SearchResultsPage searchResultsPage = new SearchResultsPage(_driver);
        Assert.IsTrue(searchResultsPage.IsResultsPageOpened());
        searchResultsPage.VerifyFlightsAirports(fromAirport, toAirport);
        searchResultsPage.SelectFlight(false, FlightType.EconomyConvenience, out int selectedFirstFlightPrice);

        searchResultsPage.VerifyFlightsAirports(toAirport, fromAirport);
        searchResultsPage
            .SelectFlight(true, FlightType.BusinessComfort, out int selectedSecondFlightPrice, true)
            .ConfirmTrip();

        SummaryPage summaryPage = new SummaryPage(_driver);
        Assert.That(summaryPage.GetTotalPrice(), Is.EqualTo(selectedFirstFlightPrice + selectedSecondFlightPrice));
        Assert.That(summaryPage.GetPassengersTotal().Contains(passengersCount.ToString()), Is.EqualTo(true));
        summaryPage.VerifyFlightDates(departureDate + " " + DateTime.Now.Year, returnDate + " " + DateTime.Now.Year);
        summaryPage.VerifyFlightsList(2, new[]
        {
            $"{fromCity} to {toCity}",
            $"{toCity} to {fromCity}"
        });
        Assert.That(summaryPage.GetPassengersCount(), Is.EqualTo(passengersCount));
    }

    [TearDown]
    public void TearDown()
    {
        _driver.Quit();
    }
}