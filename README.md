﻿Test case can be found by link: https://docs.google.com/document/d/1TsYhZN9dj6vtN25j1e2PD-VJELy4-Mh2Rb_5gR4B5hQ/edit

What is used:

- C# (.NET 7)
- Selenium

Requirements:

- .NET 7

How to run:

```
dotnet test
```

Test case:
| # | Step | Expected result | Example |
|-----|-------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------|----------|
| 1 | Go to Home page | Home page is open | |
| 2 | Fill the From field | From field has value | London |
| 3 | Fill the To field | To field has value | Tokyo |
| 4 | Select Departure date | Departure date is selected | 18 Nov |
| 5 | Select Return date | Return date is selected | 25 Nov |
| 6 | Click Passenger/Class dropdown | Passengers/Class menu is opened | |
| 7 | Set adults passengers count to 2 by clicking the + button | Adult passengers count is 2 | |
| 8 | Click Confirm | Passengers/Class menu closed and passengers count is updated to 2 | |
| 9 | Click ‘Show flights’ button | User is redirected to search results page | |
| 10 | Verify that valid flights are shown for the selected From and Departure date values | Search results have valid flights for the selected combination of From and Departure date values | |
| 11 | For the 1st flight in the list select Economy flight type | Menu with available fares is displayed | |
| 12 | Remember price of ‘Economy Convenience‘ fare type | | |
| 13 | Click ‘Select fare’ button for the ‘Economy Convenience‘ fare type | Departure selected popup is shown and results page is updated | |
| 14 | Verify that valid flights are shown for the selected To and Return date values | Search results have valid flights for the selected combination of To and Return date values | |
| 15 | For the 1st flight in the list select Business flight type | Menu with available fares is displayed | |
| 16 | Remember price of ‘Business Comfort’ fare type | | |
| 17 | Click ‘Select fare’ button for the ‘Business Comfort’ fare type | Use is redirected to ‘Who's traveling?‘ page | |
| 18 | Verify flights info | Flights info is displayed according to selected values on steps 2-5 | |
| 19 | Verify total price value | Total price is correct and equals sum of values from steps 12 and 16 | |

Notes:

- please be aware that website under test has data that may change so test may fail
- possible failure notice - button with desired flight type is not clicked which causes test to fail. Needs deeper
  investigation and building proper waiting strategies over elements

Points of improvement:

- parametrization
- building proper waiting strategy
- logging
- reporting
